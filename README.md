# SF2 Store

SF2 Store é uma aplicação que simula as funcionalidades de uma loja virtual (sem integração com formas de pagamento).
Desenvolvida em PHP, faz uso do framework Symfony2, Bootstrap e jQuery.

Com ela você pode:

*  Visualizar os itens da loja que podem estar em uma ou mais categorias
*  Buscar produtos
*  Adicionar produtos ao carrinho de compras
*  Finalizar o pedido preenchendo um formulário que salva as informações do pedido no banco de dados.

### Tecnologias envolvidas

* [PHP] - duh!
* [Symfony] - Framework PHP para projetos Web
* [Bootstrap] - Framework front-end
* [jQuery] - Biblioteca JavaScript

### Instalação

SF2 Store requer:

* [PHP] 5.3+ para executar o framework.
* [Composer] para instalar dependências.

Crie o diretório da aplicação, entre no diretório e clone o projeto:

```sh
$ cd /var/www/html
$ mkdir appdir
$ cd appdir
$ git clone git@bitbucket.org:KaduNovoK/sf2-store.git ./
```

Utilize o composer e instale o framework e suas dependencias:

```sh
$ composer install
```
Verifique as informações de acesso ao banco de dados em `app/config/parameters.yml`. Se estiverem corretas, crie o banco de dados, as tabelas e carregue as fixtures com os dados de amostra:

```sh
$ php app/console doc:dat:cre
$ php app/console doc:sch:up --force
$ php app/console doc:fix:loa
```

Limpe o cache da aplicação e acesse a aplicação no seu navegador `http://localhost/appdir/web/app.php`:

```sh
$ php php app/console cache:clear --env=prod --no-debug
```

Espero que tudo esteja certo! :D

Até logo.


   [jQuery]: <http://jquery.com>
   [PHP]: <https://secure.php.net/>
   [Symfony]: <http://symfony.com/>
   [Bootstrap]: <http://getbootstrap.com/>
   [Composer]: <https://getcomposer.org/>