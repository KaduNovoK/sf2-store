<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    /**
     * @Route("/search")
     */
    public function indexAction(Request $request)
    {
        $q = $request->query->get('search');

        $em = $this->getDoctrine()->getManager();

        $productRepository = $em->getRepository('AppBundle:Product');

        $products = $productRepository->findBySearch($q);

        $twigFile = 'search/index.html.twig';
        $twigData = array(
            'query'    => $q,
            'products' => $products,
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }
}
