<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/*
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\Image;
*/

class CategoryController extends Controller
{
    /**
     * @Route("/category/{id}")
     */
    public function indexAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $categoryRepository = $em->getRepository('AppBundle:Category');

        $categories = $categoryRepository->findBy(array('parent' => null));

        $category = $categoryRepository->find($id);

        $twigFile = 'category/index.html.twig';
        $twigData = array(
            'categories' => $categories,
            'category'   => $category,
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }
}
