<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/*
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\Image;
*/

class IndexController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $categoryRepository = $em->getRepository('AppBundle:Category');

        $categories = $categoryRepository->findBy(array('parent' => null));

        $allCategories = $categoryRepository->findBy(array(), array());

        $twigFile = 'index/index.html.twig';
        $twigData = array(
            'categories'    => $categories,
            'allCategories' => $allCategories,
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }
}
