<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IncludesController extends Controller
{
    public function mainMenuLiAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categoryRepository = $em->getRepository('AppBundle:Category');

        $categories = $categoryRepository->findBy(array('parent' => null));

        $twigFile = 'includes/main-menu-li.html.twig';
        $twigData = array(
            'categories' => $categories
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }
}
