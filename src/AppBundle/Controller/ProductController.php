<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/*
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\Image;
*/

class ProductController extends Controller
{
    /**
     * @Route("/product/{id}")
     */
    public function indexAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $productRepository = $em->getRepository('AppBundle:Product');

        $product = $productRepository->find($id);

        $twigFile = 'product/index.html.twig';
        $twigData = array(
            'product'   => $product,
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }
}
