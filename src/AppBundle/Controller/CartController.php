<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\PurchaseItem;
use AppBundle\Form\PurchaseType;
use AppBundle\Entity\Purchase;

/**
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();

        $products = null;

        if ($session->get('products')) {
            $products = $session->get('products');
        }

        $twigFile = 'cart/index.html.twig';
        $twigData = array(
            'products' => $products,
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }

    /**
     * @Route("/add")
     */
    public function addAction(Request $request)
    {
        $productId = $request->query->get('product');
        $quantity  = $request->query->get('quantity');

        $session = $request->getSession();

        $products = array();

        if ($session->get('products')) {
            $products = $session->get('products');
        }

        if (isset($products[$productId])) {
            $products[$productId]['quantity'] += $quantity;
        } else {
            $em = $this->getDoctrine()->getManager();

            $productRepository = $em->getRepository('AppBundle:Product');

            $product = $productRepository->find($productId);

            $products[$product->getId()] = array(
                'image'     => $product->getImages()[0]->getName(),
                'name'      => $product->getName(),
                'price'     => $product->getPrice(),
                'quantity'  => $quantity,
            );            
        }

        $session->set('products', $products);

        return $this->redirectToRoute('app_cart_index');
    }

    /**
     * @Route("/remove")
     */
    public function removeAction(Request $request)
    {
        $productId = $request->query->get('product');
        //$quantity  = $request->query->get('quantity');

        $session = $request->getSession();

        $products = $session->get('products');

        if ($products[$productId]) {
            //$products[$productId]['quantity'] -= $quantity;
            $products[$productId]['quantity'] -= 1;

            if ($products[$productId]['quantity'] <= 0) {
                unset($products[$productId]);
            }
        }

        $session->set('products', $products);

        return $this->redirectToRoute('app_cart_index');
    }

    /**
     * @Route("/clear")
     */
    public function clearAction(Request $request)
    {
        $session = $request->getSession();

        $session->set('products', array());

        return $this->redirectToRoute('app_cart_index');
    }

    /**
     * @Route("/checkout")
     */
    public function checkoutAction(Request $request)
    {
        $session = $request->getSession();

        $products = $session->get('products');

        $purchase = new Purchase();

        $totalPrice = 0;

        foreach ($products as $id => $product) {

            $totalPrice += $product['quantity'] * $product['price'];

            $purchaseItem = new PurchaseItem();

            $productRepository = $this->getDoctrine()->getRepository('AppBundle:Product');

            $productObj = $productRepository->find($id);

            $purchaseItem->setQuantity($product['quantity']);            
            $purchaseItem->setValue($product['price']);
            $purchaseItem->setProduct($productObj);
            $purchaseItem->setPurchase($purchase);

            $purchase->addPurchaseItem($purchaseItem);
        }

        $purchase->setValue($totalPrice);

        $form = $this->createForm(PurchaseType::class, $purchase);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $purchase->setCreatedAt(new \Datetime);

            $em = $this->getDoctrine()->getManager();

            $em->persist($purchase);
            $em->flush();

            $session->set('products', array());

            return $this->redirectToRoute('app_cart_success');
        }

        $twigFile = 'cart/checkout.html.twig';
        $twigData = array(
            'form' => $form->createView(),
        );

        return $this->render(
            $twigFile,
            $twigData
        );
    }

    /**
     * @Route("/success")
     */
    public function successAction(Request $request)
    {
        $twigFile = 'cart/success.html.twig';
        $twigData = array();

        return $this->render(
            $twigFile,
            $twigData
        );
    }
}
