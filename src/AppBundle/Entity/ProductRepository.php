<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findBySearch($search)
    {
    	$query = $this
    		->getEntityManager()
    		->createQuery('SELECT p FROm AppBundle:Product p WHERE p.name LIKE :name OR p.description LIKE :description OR p.features LIKE :features')
    		->setParameters(array(
    			'name' 			=> '%'.$search.'%',
    			'description' 	=> '%'.$search.'%',
    			'features' 		=> '%'.$search.'%',
    		))
    	;

    	return $query->getResult();
    }
}