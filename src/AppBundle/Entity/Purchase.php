<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="purchase")
 */
class Purchase
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Campo requerido")
     * @Assert\Length(
     *      min = 11,
     *      max = 11,
     *      exactMessage = "Campo com 11 digitos"
     * )
     */
    private $cpf;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Preencha o campo email")
     * @Assert\Email(message="Campo requerido")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, name="address_street")
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $addressStreet;

    /**
     * @ORM\Column(type="string", length=255, name="address_number")
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $addressNumber;

    /**
     * @ORM\Column(type="string", length=255, name="address_cep")
     * @Assert\NotBlank(message="Campo requerido")
     * @Assert\Length(
     *      min = 8,
     *      max = 8,
     *      exactMessage = "Campo com 8 digitos"
     * )
     */
    private $addressCep;

    /**
     * @ORM\Column(type="string", length=255, name="address_neighborhood")
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $addressNeighborhood;    

    /**
     * @ORM\Column(type="string", length=255, name="address_city")
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $addressCity;

    /**
     * @ORM\Column(type="string", length=255, name="address_state")
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $addressState;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank(message="Campo requerido")
     */
    private $value;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Campo requerido")
     * @Assert\Choice(
     *     choices = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
     *     message = "Campo requerido"
     * )
     */
    private $installments;

    /**
     *  @ORM\Column(type="datetime", name="created_at")
     */
     protected $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="PurchaseItem", mappedBy="purchase", cascade = {"all"})
     */
    private $purchaseItems;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->purchaseItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Purchase
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cpf
     *
     * @param string $cpf
     * @return Purchase
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;

        return $this;
    }

    /**
     * Get cpf
     *
     * @return string 
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Purchase
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set addressStreet
     *
     * @param string $addressStreet
     * @return Purchase
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;

        return $this;
    }

    /**
     * Get addressStreet
     *
     * @return string 
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * Set addressNumber
     *
     * @param integer $addressNumber
     * @return Purchase
     */
    public function setAddressNumber($addressNumber)
    {
        $this->addressNumber = $addressNumber;

        return $this;
    }

    /**
     * Get addressNumber
     *
     * @return integer 
     */
    public function getAddressNumber()
    {
        return $this->addressNumber;
    }

    /**
     * Set addressCep
     *
     * @param string $addressCep
     * @return Purchase
     */
    public function setAddressCep($addressCep)
    {
        $this->addressCep = $addressCep;

        return $this;
    }

    /**
     * Get addressCep
     *
     * @return string 
     */
    public function getAddressCep()
    {
        return $this->addressCep;
    }

    /**
     * Set addressNeighborhood
     *
     * @param string $addressNeighborhood
     * @return Purchase
     */
    public function setAddressNeighborhood($addressNeighborhood)
    {
        $this->addressNeighborhood = $addressNeighborhood;

        return $this;
    }

    /**
     * Get addressNeighborhood
     *
     * @return string 
     */
    public function getAddressNeighborhood()
    {
        return $this->addressNeighborhood;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     * @return Purchase
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string 
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     * @return Purchase
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string 
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Purchase
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set installments
     *
     * @param integer $installments
     * @return Purchase
     */
    public function setInstallments($installments)
    {
        $this->installments = $installments;

        return $this;
    }

    /**
     * Get installments
     *
     * @return integer 
     */
    public function getInstallments()
    {
        return $this->installments;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Purchase
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add purchaseItems
     *
     * @param \AppBundle\Entity\PurchaseItem $purchaseItems
     * @return Purchase
     */
    public function addPurchaseItem(\AppBundle\Entity\PurchaseItem $purchaseItems)
    {
        $this->purchaseItems[] = $purchaseItems;

        return $this;
    }

    /**
     * Remove purchaseItems
     *
     * @param \AppBundle\Entity\PurchaseItem $purchaseItems
     */
    public function removePurchaseItem(\AppBundle\Entity\PurchaseItem $purchaseItems)
    {
        $this->purchaseItems->removeElement($purchaseItems);
    }

    /**
     * Get purchaseItems
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPurchaseItems()
    {
        return $this->purchaseItems;
    }
}
