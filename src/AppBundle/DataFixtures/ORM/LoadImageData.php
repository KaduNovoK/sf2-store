<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Image;

class LoadImageData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		$sofa0101 = new Image();

		$sofa0101->setName('sofa1-1.jpg');
		$sofa0101->setProduct($this->getReference('sofa1Product'));

		$manager->persist($sofa0101);
		$manager->flush();

		$this->addReference('sofa0101Image', $sofa0101);

		$sofa0102 = new Image();

		$sofa0102->setName('sofa1-2.jpg');
		$sofa0102->setProduct($this->getReference('sofa1Product'));

		$manager->persist($sofa0102);
		$manager->flush();

		$this->addReference('sofa0102Image', $sofa0102);

		$sofa0201 = new Image();

		$sofa0201->setName('sofa2-1.jpg');
		$sofa0201->setProduct($this->getReference('sofa2Product'));

		$manager->persist($sofa0201);
		$manager->flush();

		$this->addReference('sofa0201Image', $sofa0201);

		$sofa0202 = new Image();

		$sofa0202->setName('sofa2-2.jpg');
		$sofa0202->setProduct($this->getReference('sofa2Product'));

		$manager->persist($sofa0202);
		$manager->flush();

		$this->addReference('sofa0202Image', $sofa0202);

		$sofa0203 = new Image();

		$sofa0203->setName('sofa2-3.jpg');
		$sofa0203->setProduct($this->getReference('sofa2Product'));

		$manager->persist($sofa0203);
		$manager->flush();

		$this->addReference('sofa0203Image', $sofa0203);

		$sofa0301 = new Image();

		$sofa0301->setName('sofa3-1.jpg');
		$sofa0301->setProduct($this->getReference('sofa3Product'));

		$manager->persist($sofa0301);
		$manager->flush();

		$this->addReference('sofa0301Image', $sofa0301);

		$sofa0302 = new Image();

		$sofa0302->setName('sofa3-2.jpg');
		$sofa0302->setProduct($this->getReference('sofa3Product'));

		$manager->persist($sofa0302);
		$manager->flush();

		$this->addReference('sofa0302Image', $sofa0302);

		$sofa0303 = new Image();

		$sofa0303->setName('sofa3-3.jpg');
		$sofa0303->setProduct($this->getReference('sofa3Product'));

		$manager->persist($sofa0303);
		$manager->flush();

		$this->addReference('sofa0303Image', $sofa0303);

		$sofa0401 = new Image();

		$sofa0401->setName('sofa4-1.jpg');
		$sofa0401->setProduct($this->getReference('sofa4Product'));

		$manager->persist($sofa0401);
		$manager->flush();

		$this->addReference('sofa0401Image', $sofa0401);

		$sofa0402 = new Image();

		$sofa0402->setName('sofa4-2.jpg');
		$sofa0402->setProduct($this->getReference('sofa4Product'));

		$manager->persist($sofa0402);
		$manager->flush();

		$this->addReference('sofa0402Image', $sofa0402);

		$table0101 = new Image();

		$table0101->setName('table1-1.jpg');
		$table0101->setProduct($this->getReference('table1Product'));

		$manager->persist($table0101);
		$manager->flush();

		$this->addReference('table0101Image', $table0101);

		$table0102 = new Image();

		$table0102->setName('table1-2.jpg');
		$table0102->setProduct($this->getReference('table1Product'));

		$manager->persist($table0102);
		$manager->flush();

		$this->addReference('table0102Image', $table0102);

		$table0103 = new Image();

		$table0103->setName('table1-3.jpg');
		$table0103->setProduct($this->getReference('table1Product'));

		$manager->persist($table0103);
		$manager->flush();

		$this->addReference('table0103Image', $table0103);

		$table0201 = new Image();

		$table0201->setName('table2-1.jpg');
		$table0201->setProduct($this->getReference('table2Product'));

		$manager->persist($table0201);
		$manager->flush();

		$this->addReference('table0201Image', $table0201);

		$table0301 = new Image();

		$table0301->setName('table3-1.jpg');
		$table0301->setProduct($this->getReference('table3Product'));

		$manager->persist($table0301);
		$manager->flush();

		$this->addReference('table0301Image', $table0301);

		$table0401 = new Image();

		$table0401->setName('table4-1.jpg');
		$table0401->setProduct($this->getReference('table4Product'));

		$manager->persist($table0401);
		$manager->flush();

		$this->addReference('table0401Image', $table0401);

		$table0402 = new Image();

		$table0402->setName('table4-2.jpg');
		$table0402->setProduct($this->getReference('table4Product'));

		$manager->persist($table0402);
		$manager->flush();

		$this->addReference('table0402Image', $table0402);

		$table0403 = new Image();

		$table0403->setName('table4-3.jpg');
		$table0403->setProduct($this->getReference('table4Product'));

		$manager->persist($table0403);
		$manager->flush();

		$this->addReference('table0403Image', $table0403);

		$coffeMachine0101 = new Image();

		$coffeMachine0101->setName('coffeMachine1-1.jpg');
		$coffeMachine0101->setProduct($this->getReference('coffeMachine1Product'));

		$manager->persist($coffeMachine0101);
		$manager->flush();

		$this->addReference('coffeMachine0101Image', $coffeMachine0101);

		$coffeMachine0102 = new Image();

		$coffeMachine0102->setName('coffeMachine1-2.jpg');
		$coffeMachine0102->setProduct($this->getReference('coffeMachine1Product'));

		$manager->persist($coffeMachine0102);
		$manager->flush();

		$this->addReference('coffeMachine0102Image', $coffeMachine0102);

		$coffeMachine0103 = new Image();

		$coffeMachine0103->setName('coffeMachine1-3.jpg');
		$coffeMachine0103->setProduct($this->getReference('coffeMachine1Product'));

		$manager->persist($coffeMachine0103);
		$manager->flush();

		$this->addReference('coffeMachine0103Image', $coffeMachine0103);

		$coffeMachine0201 = new Image();

		$coffeMachine0201->setName('coffeMachine2-1.jpg');
		$coffeMachine0201->setProduct($this->getReference('coffeMachine2Product'));

		$manager->persist($coffeMachine0201);
		$manager->flush();

		$this->addReference('coffeMachine0201Image', $coffeMachine0201);

		$coffeMachine0202 = new Image();

		$coffeMachine0202->setName('coffeMachine2-2.jpg');
		$coffeMachine0202->setProduct($this->getReference('coffeMachine2Product'));

		$manager->persist($coffeMachine0202);
		$manager->flush();

		$this->addReference('coffeMachine0202Image', $coffeMachine0202);

		$coffeMachine0203 = new Image();

		$coffeMachine0203->setName('coffeMachine2-3.jpg');
		$coffeMachine0203->setProduct($this->getReference('coffeMachine2Product'));

		$manager->persist($coffeMachine0203);
		$manager->flush();

		$this->addReference('coffeMachine0203Image', $coffeMachine0203);

		$coffeMachine0301 = new Image();

		$coffeMachine0301->setName('coffeMachine3-1.jpg');
		$coffeMachine0301->setProduct($this->getReference('coffeMachine3Product'));

		$manager->persist($coffeMachine0301);
		$manager->flush();

		$this->addReference('coffeMachine0301Image', $coffeMachine0301);

		$coffeMachine0302 = new Image();

		$coffeMachine0302->setName('coffeMachine3-2.jpg');
		$coffeMachine0302->setProduct($this->getReference('coffeMachine3Product'));

		$manager->persist($coffeMachine0302);
		$manager->flush();

		$this->addReference('coffeMachine0302Image', $coffeMachine0302);

		$coffeMachine0303 = new Image();

		$coffeMachine0303->setName('coffeMachine3-3.jpg');
		$coffeMachine0303->setProduct($this->getReference('coffeMachine3Product'));

		$manager->persist($coffeMachine0303);
		$manager->flush();

		$this->addReference('coffeMachine0303Image', $coffeMachine0303);

		$coffeMachine0401 = new Image();

		$coffeMachine0401->setName('coffeMachine4-1.jpg');
		$coffeMachine0401->setProduct($this->getReference('coffeMachine4Product'));

		$manager->persist($coffeMachine0401);
		$manager->flush();

		$this->addReference('coffeMachine0401Image', $coffeMachine0401);

		$coffeMachine0402 = new Image();

		$coffeMachine0402->setName('coffeMachine4-2.jpg');
		$coffeMachine0402->setProduct($this->getReference('coffeMachine4Product'));

		$manager->persist($coffeMachine0402);
		$manager->flush();

		$this->addReference('coffeMachine0402Image', $coffeMachine0402);

		$coffeMachine0403 = new Image();

		$coffeMachine0403->setName('coffeMachine4-3.jpg');
		$coffeMachine0403->setProduct($this->getReference('coffeMachine4Product'));

		$manager->persist($coffeMachine0403);
		$manager->flush();

		$this->addReference('coffeMachine0403Image', $coffeMachine0403);

		$hairdryer0101 = new Image();

		$hairdryer0101->setName('hairdryer1-1.jpg');
		$hairdryer0101->setProduct($this->getReference('hairdryer1Product'));

		$manager->persist($hairdryer0101);
		$manager->flush();

		$this->addReference('hairdryer0101Image', $hairdryer0101);

		$hairdryer0102 = new Image();

		$hairdryer0102->setName('hairdryer1-2.jpg');
		$hairdryer0102->setProduct($this->getReference('hairdryer1Product'));

		$manager->persist($hairdryer0102);
		$manager->flush();

		$this->addReference('hairdryer0102Image', $hairdryer0102);

		$hairdryer0103 = new Image();

		$hairdryer0103->setName('hairdryer1-3.jpg');
		$hairdryer0103->setProduct($this->getReference('hairdryer1Product'));

		$manager->persist($hairdryer0103);
		$manager->flush();

		$this->addReference('hairdryer0103Image', $hairdryer0103);

		$hairdryer0201 = new Image();

		$hairdryer0201->setName('hairdryer2-1.jpg');
		$hairdryer0201->setProduct($this->getReference('hairdryer2Product'));

		$manager->persist($hairdryer0201);
		$manager->flush();

		$this->addReference('hairdryer0201Image', $hairdryer0201);

		$hairdryer0301 = new Image();

		$hairdryer0301->setName('hairdryer3-1.jpg');
		$hairdryer0301->setProduct($this->getReference('hairdryer3Product'));

		$manager->persist($hairdryer0301);
		$manager->flush();

		$this->addReference('hairdryer0301Image', $hairdryer0301);

		$hairdryer0401 = new Image();

		$hairdryer0401->setName('hairdryer4-1.jpg');
		$hairdryer0401->setProduct($this->getReference('hairdryer4Product'));

		$manager->persist($hairdryer0401);
		$manager->flush();

		$this->addReference('hairdryer0401Image', $hairdryer0401);
	}

	public function getOrder()
	{
		return 3;
	}
}