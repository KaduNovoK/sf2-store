<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Category;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		// Móveis
		$furniture = new Category();

		$furniture->setName('Móveis');

		$manager->persist($furniture);
		$manager->flush();

		$this->addReference('furnitureCategory', $furniture);

		// Sala de Estar
		$livingRoom = new Category();

		$livingRoom->setName('Sala de Estar');
		$livingRoom->setParent($furniture);

		$manager->persist($livingRoom);
		$manager->flush();

		$this->addReference('livingRoomCategory', $livingRoom);

		$sofa = new Category();

		// Sofás
		$sofa->setName('Sofás');
		$sofa->setParent($livingRoom);

		$manager->persist($sofa);
		$manager->flush();

		$this->addReference('sofaCategory', $sofa);

		// Mesas de Centro
		$centerTable = new Category();

		$centerTable->setName('Mesas de Centro');
		$centerTable->setParent($livingRoom);

		$manager->persist($centerTable);
		$manager->flush();

		$this->addReference('centerTableCategory', $centerTable);

		// Eletrodomésticos
		$homeAppliance = new Category();

		$homeAppliance->setName('Eletrodomésticos');

		$manager->persist($homeAppliance);
		$manager->flush();

		$this->addReference('homeApplianceCategory', $homeAppliance);

		// Cozinha
		$kitchen = new Category();

		$kitchen->setName('Cozinha');
		$kitchen->setParent($homeAppliance);

		$manager->persist($kitchen);
		$manager->flush();

		$this->addReference('kitchenCategory', $kitchen);

		// Cafeteira
		$coffeMachine = new Category();

		$coffeMachine->setName('Cafeteiras');
		$coffeMachine->setParent($kitchen);

		$manager->persist($coffeMachine);
		$manager->flush();

		$this->addReference('coffeMachineCategory', $coffeMachine);

		// Higiene
		$hygiene = new Category();

		$hygiene->setName('Higiene');
		$hygiene->setParent($homeAppliance);

		$manager->persist($hygiene);
		$manager->flush();

		$this->addReference('hygieneCategory', $hygiene);

		// Secador de cabelo
		$hairdryer = new Category();

		$hairdryer->setName('Secador de cabelo');
		$hairdryer->setParent($hygiene);

		$manager->persist($hairdryer);
		$manager->flush();

		$this->addReference('hairdryerCategory', $hairdryer);

		// Mais Vendidos
		$besttSellers = new Category();

		$besttSellers->setName('Mais Vendidos');

		$manager->persist($besttSellers);
		$manager->flush();

		$this->addReference('bestSellersCategory', $besttSellers);
	}

	public function getOrder()
	{
		return 1;
	}
}