<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Product;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		// Mesa Centro Isis Oval Bege
		$table1 = new Product();

		$table1->setName('Mesa Centro Isis Oval Bege');
		$table1->setDescription('
			Deixe sua sala ainda mais linda e organizada com esta beleza de Mesa Centro Isis! Sofisticada e chique, ela vai complementar a decoração o seu lar e deixá-lo ainda mais encantador! Com design moderno, ela possui espaço para você colocar revistas, jornais ou livros. Você ainda pode utilizá-la para colocar objetos de decoração ou a pipoca, quando estiver assistindo aquele filminho preferido! Amei e você?
		');
		$table1->setFeatures('
			<table width="100%">
                <tbody>
                	<tr>
        				<td>
        					<i class="dimensions-icon product_height-icon"></i>
        					<strong>Altura: </strong>
            				<span class="dynamic_option_product_height">40 cm</span>
    					</td>
    
        				<td>
					        <i class="dimensions-icon product_width-icon"></i>
					        <strong>Largura: </strong>
					        <span class="dynamic_option_product_width">80 cm</span>
					    </td>
    
        				<td style="display: ;">
					        <i class="dimensions-icon product_length-icon"></i>
					        <strong>Profundidade: </strong>
					        <span class="dynamic_option_product_length">45 cm</span>
					    </td>
    
    				    <td>
					        <i class="dimensions-icon product_weight-icon"></i>
					        <strong>Peso: </strong>
					        <span class="dynamic_option_product_weight">7,500 kg</span>
					    </td>
    				</tr>

					<tr class="special-attributes">
					    <th width="40%">Altura</th>
					    <td width="60%">
					        <span class="dynamic_option_product_height">40 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">Largura</th>
					    <td width="60%">
					        <span class="dynamic_option_product_width">80 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes" style="display: ;">
					    <th width="40%">Profundidade</th>
					    <td width="60%">
					        <span class="dynamic_option_product_length">45 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">Peso</th>
					    <td width="60%">
					        <span class="dynamic_option_product_weight">7,500 kg</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">SKU</th>
					    <td width="60%">AR327TA96IBNMOB</td>
					</tr>

					<tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Bege</span>
                        </td>
                    </tr>
                    
                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">3 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Mesa</span>
                        </td>
                    </tr>
					
					<tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">Isis</span>
                        </td>
                    </tr>
					
					<tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Base: MDF Pintura UV Texturizada / Tampo: MDF Pintura UV Texturizada</span>
                        </td>
                    </tr>
					
					<tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">40X80X45</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Sim, sugerimos a contratação do nosso serviço de montagem (para os CEPs em que o serviço está disponível) ou de algum profissional experiente de sua preferência</span>
                        </td>
                    </tr>
					
					<tr class=" even">
                        <th width="40%" class="strong">Quantidade de Peças</th>
                        <td width="60%">
                            <span class="dynamic_option_number_of_pieces">1 peça</span>
                        </td>
                    </tr>
					
					<tr class=" odd">
                        <th width="40%" class="strong">Formato</th>
                        <td width="60%">
                            <span class="dynamic_option_format_gl">Outro formato</span>
                        </td>
                    </tr>
					
					<tr class=" even">
                        <th width="40%" class="strong">Regulagem de Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_adjustable_height_ta">Não</span>
                        </td>
                    </tr>
                </tbody>
            </table>
		');
		$table1->setPrice(69.99);

		$table1->addCategory($this->getReference('centerTableCategory'));
        $table1->addCategory($this->getReference('bestSellersCategory'));

		$manager->persist($table1);
		$manager->flush();

		$this->addReference('table1Product', $table1);

		// Mesa de Centro Saara Branco,Preto
		$table2 = new Product();

		$table2->setName('Mesa de Centro Saara Branco,Preto');
		$table2->setDescription('
			A Mesa de Centro Saara é produzida pensando em enriquecer sua sala de estar com conforto e design decorativo.

			Fabricada em MDF de alto padrão e qualidade com acabamento em pintura UV de alto brilho, obtendo resistência, beleza e praticidade para o produto. Sua medida é de 33 x 63 x 63 cm, proporcionando facilidade para colocar a mesa no local de sua preferência.

			A Mesa de Centro Saara conta com a qualidade Artely, a empresa que é líder no segmento de complementos para sala, sendo a marca nacional mais lembrada nessa categoria.
		');
		$table2->setFeatures('
			<table width="100%">
                <tbody>
                	<tr>
        				<td>
					        <i class="dimensions-icon product_height-icon"></i>
					        <strong>Altura: </strong>
					        <span class="dynamic_option_product_height">34 cm</span>
					    </td>
    
        				<td>
					        <i class="dimensions-icon product_width-icon"></i>
					        <strong>Largura: </strong>
					        <span class="dynamic_option_product_width">63 cm</span>
					    </td>
    
        				<td style="display: ;">
					        <i class="dimensions-icon product_length-icon"></i>
					        <strong>Profundidade: </strong>
					        <span class="dynamic_option_product_length">63 cm</span>
					    </td>
    
        				<td>
					        <i class="dimensions-icon product_weight-icon"></i>
					        <strong>Peso: </strong>
					        <span class="dynamic_option_product_weight">15,800 kg</span>
					    </td>
    				</tr>

					<tr class="special-attributes">
					    <th width="40%">Altura</th>
					    <td width="60%">
					        <span class="dynamic_option_product_height">34 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">Largura</th>
					    <td width="60%">
					        <span class="dynamic_option_product_width">63 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes" style="display: ;">
					    <th width="40%">Profundidade</th>
					    <td width="60%">
					        <span class="dynamic_option_product_length">63 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">Peso</th>
					    <td width="60%">
					        <span class="dynamic_option_product_weight">15,800 kg</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">SKU</th>
					    <td width="60%">AR327TA44OAP</td>
					</tr>

					<tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Branco</span>
                        </td>
                    </tr>

					<tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">6 Meses</span>
                        </td>
                    </tr>

					<tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Mesa de Centro Saara</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">Saara</span>
                        </td>
                    </tr>
					
					<tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Base: MDP Pintura UV Texturizada / Tampo: MDP Pintura UV Texturizada</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">33x63x63</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Sim, sugerimos a contratação do nosso serviço de montagem (para os CEPs em que o serviço está disponível) ou de algum profissional experiente de sua preferência</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Quantidade de Peças</th>
                        <td width="60%">
                            <span class="dynamic_option_number_of_pieces">1 peça</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Formato</th>
                        <td width="60%">
                            <span class="dynamic_option_format_gl">Quadrado</span>
                        </td>
                    </tr>
                </tbody>
            </table>
		');
		$table2->setPrice(99.99);

		$table2->addCategory($this->getReference('centerTableCategory'));

		$manager->persist($table2);
		$manager->flush();

		$this->addReference('table2Product', $table2);

        // Mesa Global Tripé 1,10 - Imbuia
        $table3 = new Product();

        $table3->setName('Mesa Global Tripé 1,10 - Imbuia');
        $table3->setDescription('
            Mesa Global Tripé Wood Prime ,Fabricada em MDF e madeira de reflorestamento. Peça de cunho decorativo devido as sua forma curvilínea e arredondada. Ideal para serem usadas em refeições, jogos com os amigos, ou em recepções.
        ');
        $table3->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">42 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">110 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">110 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">42,000 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">WO015UP46PPDMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Imbuia</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">03 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">262425</span>
                        </td>
                    </tr>
                    
                    <tr class=" even">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Madeira Maciça</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">42 x 110 x 110</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Formato</th>
                        <td width="60%">
                            <span class="dynamic_option_format_gl">Redondo</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $table3->setPrice(2019.90);

        $table3->addCategory($this->getReference('centerTableCategory'));

        $manager->persist($table3);
        $manager->flush();

        $this->addReference('table3Product', $table3);

        // Mesa de Centro Madras Rico Mel
        $table4 = new Product();

        $table4->setName('Mesa de Centro Madras Rico Mel');
        $table4->setDescription('
            Para deixar sua sala ainda mais encantadora e organizada, a Mesa de Centro Madras é a melhor opção! Fabricada em madeira maciça de alta qualidade, a mesa de centro possui um design rústico que ficaria encantador em sua sala! O tom mel escuro do móvel agrega uma aparência doce e leve ao ambiente, combinando com toda a decoração e dando um toque super delicado! Diferente e maravilhosa, não é? =)
        ');
        $table4->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">40 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">70 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">110 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">34,000 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">JA046TA64SIDMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Mel</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">3 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Mesa de Centro</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">Madras</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Madeira maciça natural</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">Altura 40 cm Largura 70 cm Comprimento 110 cm</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Quantidade de Peças</th>
                        <td width="60%">
                            <span class="dynamic_option_number_of_pieces">1 peça</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Formato</th>
                        <td width="60%">
                            <span class="dynamic_option_format_gl">Retangular</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $table4->setPrice(1039.99);

        $table4->addCategory($this->getReference('centerTableCategory'));
        $table4->addCategory($this->getReference('bestSellersCategory'));

        $manager->persist($table4);
        $manager->flush();

        $this->addReference('table4Product', $table4);

		// Sofá 3 Lugares Retrátil Fluence Suede Bege
		$sofa1 = new Product();

		$sofa1->setName('Sofá 3 Lugares Retrátil Fluence Suede Bege');
		$sofa1->setDescription('
			O sofá é o principal móvel da sala e o que torna o cômodo receptivo e aconchegante, certo? Por isso ele deve ser confortável, para que você possa relaxar enquanto vê TV ou lê um livro. O Sofá Fluence de 3 lugares, acomoda a família, oferecendo o descanso merecido após um dia cansativo.

			Seu design contemporâneo compõe muito bem um ambiente, caso a sua ideia seja simplicidade e funcionalidade.
		');
		$sofa1->setFeatures('
			<table width="100%">
                <tbody>
                	<tr>
        				<td>
        					<i class="dimensions-icon product_height-icon"></i>
        					<strong>Altura: </strong>
            				<span class="dynamic_option_product_height">92 cm</span>
    					</td>
    
        				<td>
        					<i class="dimensions-icon product_width-icon"></i>
        					<strong>Largura: </strong>
        					<span class="dynamic_option_product_width">234 cm</span>
    					</td>
    
        				<td style="display: ;">
        					<i class="dimensions-icon product_length-icon"></i>
        					<strong>Profundidade: </strong>
        					<span class="dynamic_option_product_length">92 cm</span>
    					</td>
    
        				<td>
        					<i class="dimensions-icon product_weight-icon"></i>
        					<strong>Peso: </strong>
        					<span class="dynamic_option_product_weight">61,000 kg</span>
    					</td>
    				</tr>

					<tr class="special-attributes">
					    <th width="40%">Altura</th>
					    <td width="60%">
					        <span class="dynamic_option_product_height">92 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">Largura</th>
					    <td width="60%">
					        <span class="dynamic_option_product_width">234 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes" style="display: ;">
					    <th width="40%">Profundidade</th>
					    <td width="60%">
					        <span class="dynamic_option_product_length">92 cm</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">Peso</th>
					    <td width="60%">
					        <span class="dynamic_option_product_weight">61,000 kg</span>
					    </td>
					</tr>

					<tr class="special-attributes">
					    <th width="40%">SKU</th>
					    <td width="60%">RI838UP45ZJQMOB</td>
					</tr>

					<tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Bege</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">3 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Instruções/Cuidados</th>
                        <td width="60%">
                            <span class="dynamic_option_product_instructions">Não Arrastar, Não Empilhar, Não Molhar, Evitar Objetos Pontiagudos, Não Deixar Exposto Ao Sol E Produtos Quimicos.</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Sofá</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">Fluence</span>
                        </td>
                    </tr>

                    <tr class=" even">
	                    <th width="40%" class="strong">Material</th>
	                    <td width="60%">
	                        <span class="dynamic_option_material">Estrutura madeira pinus e eucalipto tratada e seca, percinta assento elástica, percinta do encosto elástica, espumas assento D-26, fibra de silicone e Braço D-26, pés de plástico, tecido suede.</span>
	                    </td>
	                </tr>

	                <tr class=" odd">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">Altura: 92 cm Largura: 234 cm Profundidade Fechado: 92 cm Profundidade Aberto: 115 cm</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Tipo de Encosto</th>
                        <td width="60%">
                            <span class="dynamic_option_type_backrest_up">Fixo</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Estrutura do Assento</th>
                        <td width="60%">
                            <span class="dynamic_option_seat_structure_up">Madeira Eucalipto</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Peso suportado pelo produto (kg)</th>
                        <td width="60%">
                            <span class="dynamic_option_recommended_weight_up">480.00</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Braços Reguláveis</th>
                        <td width="60%">
                            <span class="dynamic_option_adjustable_arms_up">Não</span>
                        </td>
                    </tr>
                </tbody>
            </table>
		');
		$sofa1->setPrice(1139.99);

		$sofa1->addCategory($this->getReference('sofaCategory'));
        $sofa1->addCategory($this->getReference('bestSellersCategory'));

		$manager->persist($sofa1);
		$manager->flush();

		$this->addReference('sofa1Product', $sofa1);

        // Sofá de Canto 5 Lugares Direito Cartagena Suede Marrom
        $sofa2 = new Product();

        $sofa2->setName('Sofá de Canto 5 Lugares Direito Cartagena Suede Marrom');
        $sofa2->setDescription('
            Sabemos que você se preocupa em proporcionar conforto às suas visitas e que também gosta de aproveitar o seu momento de lazer para descansar merecidamente. Então que tal compor a sua sala de estar com o lindo Sofá de Canto Cartagena? Repare em seu design e imagine como ficaria linda a decoração do ambiente com um toque contemporâneo, cheio de charme e elegância. Suas linhas retas dão ao móvel um toque de simplicidade, ideal para quem gosta de ambientes sofisticados.
        ');
        $sofa2->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%"><span class="dynamic_option_product_height">92 cm</span></td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%"><span class="dynamic_option_product_width">376 cm</span></td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%"><span class="dynamic_option_product_length">75 cm</span></td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%"><span class="dynamic_option_product_weight">94,000 kg</span></td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">AM155UP71LZKMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%"><span class="dynamic_option_color">Marrom</span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia                            </th>
                        <td width="60%"><span class="dynamic_option_product_warranty">3 Meses</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%"><span class="dynamic_option_product_contents">2 Sofás</span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%"><span class="dynamic_option_model">AC 4800/864</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%"><span class="dynamic_option_material">Madeira reflorestamento, assento espuma D26, encosto almofadas soltas flocos de espuma, tecido suede.</span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%"><span class="dynamic_option_size_description">2 Lugares + Canto Altura: 92 cm Largura: 206 cm Profundidade: 75 cm / 2 Lugares + Puff Altura: 92 cm Largura: 170 cm Profundidade: 75 cm</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Necessita Montagem?                            </th>
                        <td width="60%"><span class="dynamic_option_assembly_required">Não</span></td>
                    </tr>
                </tbody>
            </table>
        ');
        $sofa2->setPrice(1089.99);

        $sofa2->addCategory($this->getReference('sofaCategory'));

        $manager->persist($sofa2);
        $manager->flush();

        $this->addReference('sofa2Product', $sofa2);

        // Sofá Retrô Zap 3 Lugares Amarelo
        $sofa3 = new Product();

        $sofa3->setName('Sofá Retrô Zap 3 Lugares Amarelo');
        $sofa3->setDescription('
            Que arrazo! É impossível não se apaixonar pelo Sofá Zap! Com um design retrô, muito parecido com os móveis da década de 1960, ele conta com pernas alongadas, detalhe que deixa a peça ainda mais encantadora. Seu modelo possui linhas arredondadas e envolventes, o seu acabamento em botonê, no encosto, vai agregar mais exclusividade e personalidade à decoração do seu lar. Perfeito para leituras, conversas e momentos de descontração. Um charme, não é mesmo?
        ');
        $sofa3->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%"><span class="dynamic_option_product_height">79 cm</span></td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%"><span class="dynamic_option_product_width">174 cm</span></td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%"><span class="dynamic_option_product_length">69 cm</span></td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%"><span class="dynamic_option_product_weight">33,000 kg</span></td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">DA167UP92YAPMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%"><span class="dynamic_option_color">Amarelo</span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%"><span class="dynamic_option_product_warranty">1 Ano</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%"><span class="dynamic_option_product_contents">1 Sofá</span></td>
                    </tr>
                    
                    <tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%"><span class="dynamic_option_model">7021.0006</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%"><span class="dynamic_option_material">Aço Cromado/Concha Multilaminada/Espuma D28/Tecido Sarja</span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%"><span class="dynamic_option_size_description">Altura: 79 cm Largura: 174 cm Profundidade: 69 cm</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%"><span class="dynamic_option_assembly_required">Não</span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Tipo de Encosto</th>
                        <td width="60%"><span class="dynamic_option_type_backrest_up">Fixo</span></td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Estrutura do Assento</th>
                        <td width="60%"><span class="dynamic_option_seat_structure_up">Espuma </span></td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Peso suportado pelo produto (kg)</th>
                        <td width="60%"><span class="dynamic_option_recommended_weight_up">120.00</span></td>
                    </tr>
                </tbody>
            </table>
        ');
        $sofa3->setPrice(1439.99);

        $sofa3->addCategory($this->getReference('sofaCategory'));

        $manager->persist($sofa3);
        $manager->flush();

        $this->addReference('sofa3Product', $sofa3);

        // Sofá Retrô Zap 3 Lugares Amarelo
        $sofa4 = new Product();

        $sofa4->setName('Sofá de Canto 6 Lugares com Chaise Astor Suede Marrom');
        $sofa4->setDescription('
            Deixe seu lar mais confortável e receptivo com a ajuda do Conjunto de Sofá Astor. Perfeito para complementar a sua casa e deixá-la sempre pronta para receber a visita de familiares, amigos e convidados.
        ');
        $sofa4->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">86 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">286 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">80 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">101,400 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">SO600UP75UKIMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Marrom</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">3 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Sofá de Canto</span>
                        </td>    
                    </tr>
                
                    <tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">Europa</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Madeira reflorestada de eucalipto e pinus. Percinta pneumática, Espuma Encosto D13 e Assento D20, Tecido Suede</span>
                        </td>
                    </tr>
                    
                    <tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">3 lugares canto Altura: 86 cm Largura: 190 cm Profundidade: 80 cm / 3 lugares sem braço Altura: 86 cm Largura: 134 cm Profundidade: 80 cm / Chaise Altura: 86 cm Largura: 72 cm Profundidade: 138 cm</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Tipo de Encosto</th>
                        <td width="60%">
                            <span class="dynamic_option_type_backrest_up">Fixo</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $sofa4->setPrice(1289.99);

        $sofa4->addCategory($this->getReference('sofaCategory'));
        $sofa4->addCategory($this->getReference('bestSellersCategory'));

        $manager->persist($sofa4);
        $manager->flush();

        $this->addReference('sofa4Product', $sofa4);

        // Cafeteira 6 Xícaras Estampada Branco
        $coffeMachine1 = new Product();

        $coffeMachine1->setName('Cafeteira 6 Xícaras Estampada Branco');
        $coffeMachine1->setDescription('
            Nada como saborear um delicioso cafezinho pela manhã, depois do almoço ou em qualquer outro momento, né? Com a Cafeteira Xic Estampada você poderá servir a família e os convidados com muito bom gosto, pois ela conta com um design super moderno e charmoso, sendo própria para o armazenamento de café. Incrível, né? :)
        ');
        $coffeMachine1->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">21 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">10 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">15 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">0,470 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">HE175AC83UCIMOB</td>
                    </tr>
                    
                    <tr class="special-attributes">
                        <th width="40%" class="strong">Voltagem</th>
                        <td width="60%">
                            <span class="dynamic_option_simple_product_voltage">Bivolt</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">
                            Cor                            </th>
                        <td width="60%">
                            <span class="dynamic_option_color">Branco</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">3 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 CAFETEIRA</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Ambientes</th>
                        <td width="60%">
                            <span class="dynamic_option_application">Cozinha</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">UM79-006</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">ALUMÍNIO</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">Altura: 20,5 cm  Largura: 9,5 cm  Comprimento: 14,5 cm</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $coffeMachine1->setPrice(69.99);

        $coffeMachine1->addCategory($this->getReference('coffeMachineCategory'));
        $coffeMachine1->addCategory($this->getReference('bestSellersCategory'));

        $manager->persist($coffeMachine1);
        $manager->flush();

        $this->addReference('coffeMachine1Product', $coffeMachine1);

        // Cafeteira Elétrica Britânia CP15 - Preto/Inox
        $coffeMachine2 = new Product();

        $coffeMachine2->setName('Cafeteira Elétrica Britânia CP15 - Preto/Inox');
        $coffeMachine2->setDescription('
            Uma cafeteira ideal para deixar seu dia a dia bem mais saboroso e prático! A CP15 da Britânia é uma cafeteira elétrica que produz até 15 cafezinhos de uma só vez, e com a praticidade que você precisa. Ela dispõe de jarra de vidro refratário, que pode ser levada á mesa, reservatório de água com graduação e placa aquecedora para manter o café aquecido após o preparo. A CP15 é bonita, moderna e indispensável para dar aquele toque de sabor ao seu dia a dia!
        ');
        $coffeMachine2->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">BR495HI21GHYMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">12 meses</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Instruções/Cuidados</th>
                        <td width="60%">
                            <span class="dynamic_option_product_instructions">- Capacidade para 15 cafezinhos- Filtro permanente- Porta filtro removível- Sistema corta-pingo- Jarra de vidro refratário - pode ser levada à mesa- Reservatório de água com graduação- Placa aquecedora para manter o café aquecido após o preparo- Botão luminoso liga/desliga- Colher dosadora</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">- 1 Cafeteira Elétrica Britânia CP15 - Preto/Inox- Manual de instruções- Certificado de Garantia</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Potência (W)</th>
                        <td width="60%">
                            <span class="dynamic_option_watt">500W ou mais</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $coffeMachine2->setPrice(49.90);

        $coffeMachine2->addCategory($this->getReference('coffeMachineCategory'));

        $manager->persist($coffeMachine2);
        $manager->flush();

        $this->addReference('coffeMachine2Product', $coffeMachine2);

        // Cafeteira Cadence Single Roxa 127V 450W
        $coffeMachine3 = new Product();

        $coffeMachine3->setName('Cafeteira Cadence Single Roxa 127V 450W');
        $coffeMachine3->setDescription('
            Para deixar a sua cozinha ainda mais aconchegante, você precisa de uma cafeteira moderna e de qualidade. Esta cafeteira é uma ótima opção para servir um café quente e saboroso.

            O aparelho possui colher dosadora, filtro permanente e 2 xícaras de porcelana. É fácil de usar, basta utilizar a colher medidora, escolher a quantidade de café, ligar a cafeteira e aguardar poucos minutos. Seu design diferenciado na cor roxa é muito atraente, sendo ideal como presente.

            O aparelho é fabricado pela Cadence, que possui uma vasta linha de qualidade com itens variados como cafeteiras, torradeiras, panelas elétricas, secadores, entre outros. A Cadence é uma empresa que valoriza o respeito aos clientes, consumidores e colaboradores, comprometendo-se com a segurança de seus produtos.
        ');
        $coffeMachine3->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">23 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">17 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">22 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">0,900 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">CA380AC70JRXMOB</td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%" class="strong">Voltagem</th>
                        <td width="60%">
                            <span class="dynamic_option_simple_product_voltage">110 V</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Roxo</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">12 Meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Cafeteira, 1 Colher Dosadora, 1 Filtro Permanente, 2 Xícaras De 150Ml</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Ambientes</th>
                        <td width="60%">
                            <span class="dynamic_option_application">Cozinha</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">CAF112-127</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Plástico</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">23x17x22xcm</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Potência (W)</th>
                        <td width="60%">
                            <span class="dynamic_option_watt">200 - 499 W</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $coffeMachine3->setPrice(129.99);

        $coffeMachine3->addCategory($this->getReference('coffeMachineCategory'));
        $coffeMachine3->addCategory($this->getReference('bestSellersCategory'));

        $manager->persist($coffeMachine3);
        $manager->flush();

        $this->addReference('coffeMachine3Product', $coffeMachine3);

        // Cafeteira Express - Tramontina By Breville-220 Volts
        $coffeMachine4 = new Product();

        $coffeMachine4->setName('Cafeteira Express - Tramontina By Breville-220 Volts');
        $coffeMachine4->setDescription('
            Uma das paixões nacionais do brasileiro é o café de preferência feito na hora. Para proporcionar este sabor ao seu dia-a-dia sem que isso leve muito tempo e nem perca qualidade a Cafeteira Express é o produto ideal! Conta com controle ajustável de temperatura para garantir o ponto certo de um café ideal. Além disso conta com recursos que facilitam ainda mais seu uso e proporcionam a funcionalidade que você precisa para ter um café saboroso a qualquer hora. A Cafeteira garante uma extração uniforme de cada grão do pó de café para um sabor irresistível. Ela ainda possui bandeja de aquecimento para xícaras e porta filtro em aço inox. O bico de vapor em estilo comercial contribui com o design moderno e sofisticado da Cafeteira Express. Deguste de um expresso em instantes no conforto da sua casa ou escritório. Possui 33 cm de altura 27 cm de largura e 40 cm de profundidade.
        ');
        $coffeMachine4->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">33 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">27 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">40 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">8,000 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">TR383HI71IAYMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Prata</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">2014032</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Aço inox</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">33 x 27 x 40</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $coffeMachine4->setPrice(2989.99);

        $coffeMachine4->addCategory($this->getReference('coffeMachineCategory'));

        $manager->persist($coffeMachine4);
        $manager->flush();

        $this->addReference('coffeMachine4Product', $coffeMachine4);

        // Secador De Cabelo 127V Com Função Ion SCI700-BR
        $hairdryer1 = new Product();

        $hairdryer1->setName('Secador De Cabelo 127V Com Função Ion SCI700-BR');
        $hairdryer1->setDescription('
            O Secador de Cabelo é fabricado em aço, plástico e cobre, materiais que oferecem maior durabilidade e resistência ao produto. Possui cabo dobrável e contém duas velocidades para que você obtenha melhores resultados em sua escova ou ate mesmo em uma simples secagem.

            O Secador de Cabelo é desenvolvido pela Black&Decker, empresa que busca oferecer produtos inovadores com ótima qualidade e alta performance, a fim de atender todas as necessidades dos clientes. 
        ');
        $hairdryer1->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">27 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">10 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">23 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">0,920 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">BL342AC17YOS</td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%" class="strong">Voltagem</th>
                        <td width="60%">
                            <span class="dynamic_option_simple_product_voltage">110 V</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Cor</th>
                        <td width="60%">
                            <span class="dynamic_option_color">Preto</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">12 meses</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Secador (<span style="font-family: Arial; font-size: small;">Acompanha concentrador de ar e difusor de volume)</span></span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Ambientes</th>
                        <td width="60%">
                            <span class="dynamic_option_application">Banheiro</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">SCI700-BR</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">plástico, cobre, aço, borracha</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">27x10x23</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Necessita Montagem?</th>
                        <td width="60%">
                            <span class="dynamic_option_assembly_required">Não</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Potência (W)</th>
                        <td width="60%">
                            <span class="dynamic_option_watt">500W ou mais</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $hairdryer1->setPrice(159.99);

        $hairdryer1->addCategory($this->getReference('hairdryerCategory'));
        $hairdryer1->addCategory($this->getReference('bestSellersCategory'));

        $manager->persist($hairdryer1);
        $manager->flush();

        $this->addReference('hairdryer1Product', $hairdryer1);

        // Secador de Cabelos Mondial Fashion Pink com Emissao de Ions Bivolt 1200W – Rosa
        $hairdryer2 = new Product();

        $hairdryer2->setName('Secador de Cabelos Mondial Fashion Pink com Emissao de Ions Bivolt 1200W – Rosa');
        $hairdryer2->setDescription('
            Indicado para todos os tipos de cabelo o Secador de Cabelos Mondial Fashion Pink com Emissao de Ions Bivolt 1200W – Rosa, e ideal para o seu dia a dia.Alem de ser extremamente leve e silencioso, possui duas velocidades, duas temperaturas, uma alca para pendurar, e bico direcionador para finalizar melhor o seu penteado. Nao perca tempo e compre ja o seu!
        ');
        $hairdryer2->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">MO553HI88GUTMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">12 meses</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Instruções/Cuidados</th>
                        <td width="60%">
                            <span class="dynamic_option_product_instructions">- Potencia de 1200W- Bivolt- Bocal direcionador - Cabo dobravel- 2 velocidades- 2 temperaturas- Alca para pendurar</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1- Secador de Cabelos Mondial Fashion Pink com Emissao de Ions Bivolt 1200W – Rosa</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $hairdryer2->setPrice(70.90);

        $hairdryer2->addCategory($this->getReference('hairdryerCategory'));

        $manager->persist($hairdryer2);
        $manager->flush();

        $this->addReference('hairdryer2Product', $hairdryer2);

        // Secador Beauty Beliss Conair
        $hairdryer3 = new Product();

        $hairdryer3->setName('Secador Beauty Beliss Conair');
        $hairdryer3->setDescription('
            Conair Beauty Beliss é a solução perfeita para cuidar dos seus cabelos porque ele seca, desembaraça, alisa e dá brilho em um só movimento. Sua tecnologia de Turmalina Cerâmica protege os fios, deixando-os incrivelmente sedosos e com movimento natural! Beauty Beliss ainda conta com o bico concentrador e o Jato de Ar frio, para fazer diferentes penteados e escolher o estilo que quer usar! Compacto, é perfeito para levar em suas viagens e conseguir resultados de salão, onde você estiver! Beauty Beliss conta com a qualidade CONAIR e é uma super exclusividade Polishop!2 ajustes de calor/velocidade: personalize os penteados Cabo antiderrapante: mais segurança e controle Bico concentrador: para penteados mais precisos Função Jato de ar frio: fixa o penteado 1400W de potencia Voltagem: 110v e 220v Número do Certificado: BRA 13/01399
        ');
        $hairdryer3->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">Altura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_height">30 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Largura</th>
                        <td width="60%">
                            <span class="dynamic_option_product_width">95 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes" style="display: ;">
                        <th width="40%">Profundidade</th>
                        <td width="60%">
                            <span class="dynamic_option_product_length">24 cm</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">Peso</th>
                        <td width="60%">
                            <span class="dynamic_option_product_weight">1,220 kg</span>
                        </td>
                    </tr>

                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">CO833HI11VCAMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">1 ano</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1 Beauty Beliss <br>
                            1 Pente beliss c/ 4 ajustes <br>
                            1 Acessório para alisar os cabelos<br>
                            1 Manual de instruções</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Modelo</th>
                        <td width="60%">
                            <span class="dynamic_option_model">Polishop</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Material</th>
                        <td width="60%">
                            <span class="dynamic_option_material">Plástico resistente e cerâmica</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Descrição do Tamanho</th>
                        <td width="60%">
                            <span class="dynamic_option_size_description">95 x 30 x 24</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $hairdryer3->setPrice(139.90);

        $hairdryer3->addCategory($this->getReference('hairdryerCategory'));

        $manager->persist($hairdryer3);
        $manager->flush();

        $this->addReference('hairdryer3Product', $hairdryer3);

        // Secador de Cabelos de Parede Mondial Ellegance 1500W – Prata
        $hairdryer4 = new Product();

        $hairdryer4->setName('Secador de Cabelos de Parede Mondial Ellegance 1500W – Prata');
        $hairdryer4->setDescription('
            Deixe seus penteados completamente incríveis Secador de Cabelos de Parede Mondial Ellegance 1500W – Prata.Com design sofisticado o Secador de Parede Mondial Ellegance - 1500W, é ideal para quem gosta de praticidade. Possui sistema de segurança que funciona apenas com o botão pressionado e 2 velocidades de ar. Não perca tempo e compre já o seu!
        ');
        $hairdryer4->setFeatures('
            <table width="100%">
                <tbody>
                    <tr class="special-attributes">
                        <th width="40%">SKU</th>
                        <td width="60%">MO553HI82GUZMOB</td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Garantia</th>
                        <td width="60%">
                            <span class="dynamic_option_product_warranty">12 meses</span>
                        </td>
                    </tr>

                    <tr class=" even">
                        <th width="40%" class="strong">Instruções/Cuidados</th>
                        <td width="60%">
                            <span class="dynamic_option_product_instructions">- Secador de parede- Potência de 1500 W- 2 temperaturas- Botão liga/desliga</span>
                        </td>
                    </tr>

                    <tr class=" odd">
                        <th width="40%" class="strong">Conteúdo da Embalagem</th>
                        <td width="60%">
                            <span class="dynamic_option_product_contents">1- Secador de Cabelos de Parede Mondial Ellegance 1500W – Prata</span>
                        </td>
                    </tr>
                </tbody>
            </table>
        ');
        $hairdryer4->setPrice(105.90);

        $hairdryer4->addCategory($this->getReference('hairdryerCategory'));
        $hairdryer4->addCategory($this->getReference('bestSellersCategory'));

        $manager->persist($hairdryer4);
        $manager->flush();

        $this->addReference('hairdryer4Product', $hairdryer4);
	}

	public function getOrder()
	{
		return 2;
	}
}